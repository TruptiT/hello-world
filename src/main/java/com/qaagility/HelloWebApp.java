package com.qaagility;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingListener; // Unnecessary import which can be removed to clear PMD warning
import java.util.Date; // Unnecessary import which can be removed to clear PMD warning

public class HelloWebApp extends HttpServlet {

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {

            InetAddress my_address = InetAddress.getLocalHost();
        	
            PrintWriter out = resp.getWriter();
            
            resp.setContentType("text/html");

            out.println("<html>");
            out.println("<body bgcolor=\"Aqua\">");

            out.println("<h1>Hello World from DevOps Alliance and ATA demo</h1>");
            resp.getWriter().write("<h2>Java Project for CPDOF Certification by Prashant Beniwal - Jan - 2021..\n\n\n</h2>");
            //resp.getWriter().write("Java Project for CPDOF Certifcation Program by Prashant Beniwal - Sep - 2020..\n\n\n");
	        out.println("<br><h2>Serving Container Name: " + my_address.getHostName()+"</h2>");
	        out.println("<br><h2>Serving Container IP: " + my_address.getHostAddress()+"</h2>");
           
	        out.println("</body>");
            out.println("</html>");   
            
            
      }


        public int add(int a, int b) {
            return a + b ;
        }

	public int sub(int a, int b) {
            return a - b ;
        }

	public int mul(int a, int b) {
           int i,j;
            return a * b;
        }

}
